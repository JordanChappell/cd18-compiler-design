/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: Symbol.class
 * Description: A representation of a special Symbol token
 */

import java.util.Objects;

public class Symbol {
    public static final int SVOID = 0,  SINTG = 1,  SREAL = 2,  SBOOL = 3,
                            SSTRCT = 4, SARRAY = 5, SARLIT = 6;
    
    private static final String PRINTTYPE[] = {
                            "SVOID  ",  "SINTG  ",  "SREAL  ",  "SBOOL  ",
                            "SSTRCT ",  "SARRAY ",  "SARLIT "};
    private String identifier;
    private String value;
    private int symbolType;
    private boolean constant;
    
    /*
    * CONSTRUCTORS
    */
    public Symbol() {
        identifier = null;
        value = null;
        symbolType = -1;
        constant = false;
    }
     
    public Symbol(String identifier) {
        this.identifier = identifier;
        value = null;
        symbolType = -1;
        constant = false;
    }
    
    public Symbol(String identifier, String value) {
        this.identifier = identifier;
        this.value = value;
        symbolType = -1;
        constant = false;
    }
    
    public Symbol(String identifier, int symbolType) {
        this.identifier = identifier;
        value = null;
        this.symbolType = symbolType;
        constant = false;
    }
    
    public Symbol(String identifier, String value, int symbolType) {
        this.identifier = identifier;
        this.value = value;
        if (symbolType != -1 && symbolType < 7) {
            this.symbolType = symbolType;
        }
        constant = false;
    }
    
    public Symbol(String identifier, String value, int symbolType, boolean constant) {
        this.identifier = identifier;
        this.value = value;
        if (symbolType != -1 && symbolType < 7) {
            this.symbolType = symbolType;
        }
        this.constant = constant;
    }
        
    /*
    * METHODS
    */
        
    public String getIdentifier() {
        return identifier;
    }
    
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public void setType(int symbolType) {
        if (symbolType != -1 && symbolType < 7) {
            this.symbolType = symbolType;
        }
    }
    
    public int getType() {
        return symbolType;
    }
    
    public String printType() {
        if (symbolType != -1) {
            return PRINTTYPE[symbolType];
        }
        return "";
    }
    
    public void setConstant(boolean constant) {
        this.constant = constant;
    }
    
    public boolean getConstant() {
        return constant;
    }
    
    @Override
    public boolean equals(Object object) {
        if (object instanceof Symbol) {
            if (this.identifier.equals(((Symbol)object).identifier)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(identifier, value, symbolType);
    }
    
    
}
