/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: SymbolTable.java
 * Description: a HashMap to store Symbol identity and Symbol pairs
 */

import java.util.HashMap;
import java.util.Map;

public class SymbolTable {
    //member variables
    private final HashMap<String, Symbol> table;
    
    /*
    * CONSTRUCTOR
    */    
    public SymbolTable () {
        table = new HashMap<>();
    }
    
    /*
    * METHODS
    */
    
    public void addSymbol(Symbol symbol) {
        table.put(symbol.getIdentifier(), symbol);
    }
    
    public Symbol search(Symbol symbol) {
        symbol = table.get(symbol.getIdentifier());
        return symbol;
    } 
    
    public Symbol search(String identifier) {
        Symbol symbol = table.get(identifier);
        return symbol;
    }
    
    public void printTable() {
        for (Map.Entry<String, Symbol> entry : table.entrySet()) {
            String key = entry.getKey();
            Symbol value = entry.getValue();
            System.out.println("SYMBOL ID: " + key);
            System.out.println("SYMBOL VALUE: " + value.getValue());
            System.out.println("SYMBOL TYPE: " + value.getType());
        }
    }
}
