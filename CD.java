/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: A1
 * Description: main program for Project 1
 */

import java.io.PrintWriter;
import java.util.ArrayList;

public class CD {
    public static void main(String[] args) {
        //check for correct command line arguments
        if (args.length != 1) {
            error("incorrect command line arguments");
        } 
        String filename = args[0];
        
        PrintWriter out = new PrintWriter(System.out, true);
        
        LexicalAnalyser scanner = new LexicalAnalyser(filename);
        ArrayList<Token> tokenList = new ArrayList<>();
        
        /*
        * LEXICAL ANALYSIS
        */
        Token token;
        
        while (!scanner.eof()) {
            token = scanner.getToken();
            if (token != null) {
                if (token.toString().equals("TUNDF ")) {   //check for a TUNDF token
                    error("an undefined token has been encountered, see program listing");
                }
                tokenList.add(token);   //add the token to the list of tokens for parsing
            }
            
        }
        
        while (scanner.remainingInput()) {         //buffer still has something untokenized
            token = scanner.getRemainingToken();
            if (token != null) {
                if (token.toString().equals("TUNDF ")) {   //check for a TUNDF token
                    error("an undefined token has been encountered, see program listing");
                }
                tokenList.add(token);   //add the token to the list of tokens for parsing
            }
        }
        
        //finally get TEOF
        token = scanner.eofToken();
        tokenList.add(token);       //add to the list of tokens
        
        /*
        * SYNTAX / SEMANTIC ANALYSIS
        */
        
        OutputController output = new OutputController(filename);
        
        //parse the list of tokens
        Parser parser = new Parser(tokenList, output);
        TreeNode root = parser.program();
        //print the tree returned from the parser
        //TreeNode.printTree(out, root);
        
        
        output.writeErrorCount();
    }   
    
    public static void error(String msg) {
        System.err.println("ERROR " + msg);
        System.err.println("Terminating application");
        System.exit(1);
    }
}
