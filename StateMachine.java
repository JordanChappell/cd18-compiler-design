/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: StateMachine.java
 * Description: A general class to hold the components of a state machine.
 */

import java.util.List;

public class StateMachine {
    //member variables
    private final List<Transition> transitions;     //basically the transition function
    private State current;              //what state the machine is in
    private State last;                 //the previous state
    private final State start;          //the start state of the machine
    private final State error;          //automatic error state when no transition defined
    private final State accept;         //accepting state of the machine
    
    /*
    * CONSTRUCTOR
    */
    public StateMachine(State start, List<Transition> transitions, State error, State accept) {
        this.transitions = transitions;
        this.current = start;
        this.last = start;
        this.start = start;
        this.error = error;
        this.accept = accept;
    }
    
    /*
    * METHODS
    */
    
    /*
    * Precondition: argument should be a valid string (1 character long)
    * Postcondition: moves the state to a valid one from transition function
    *                or moves to error state and return false
    */
    public boolean consume(String input) {
        if (input.length() > 1) {
            System.err.println("FATAL ERROR, EXITING PROGRAM");
            System.exit(1);
        }
        for (Transition transition : transitions) {
            if (transition.getFrom().equals(current)) {
                for (String condition : transition.getConditions()) {
                    if (condition.equals(input.toLowerCase())) {
                        last = current;
                        current = transition.getTo();
                        return true;
                    } 
                }
            }
        }
        last = current;
        current = error;
        return false;
    } 

    public State currentState() {
        return current;
    }
    
    public State lastState() {
        return last;
    }
    
    public boolean inError() {
        if (current == error) {
            return true;
        }
        return false;
    }
    
    public boolean inAccept() {
        if (current == accept) {
            return true;
        }
        return false;
    }
    
    public void reset() {
        current = start;
    }
    
    
}
