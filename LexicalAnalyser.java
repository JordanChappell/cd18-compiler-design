/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: LexicalAnalyser
 * Description: the main driver of the project, returns the next token on command, handles printing of tokens
 */

import java.util.ArrayList;

public class LexicalAnalyser {
    //member variables
    private final InputController inputController;
    private final OutputController outputController;
    private final Cursor cursor;
    private final StateMachine machine;
    private final MachineBuilder builder;
    private final ArrayList<String> buffer;
    private final Tokenizer tokenizer;
    private boolean remainingInputFlag;
    
    /*
    * CONSTRUCTOR
    */
    public LexicalAnalyser(String filename) {
        inputController = new InputController(filename);
        outputController = new OutputController(filename);
        cursor = new Cursor();
        builder = new MachineBuilder();
        machine = builder.getMachine();
        buffer = new ArrayList<>();
        tokenizer = new Tokenizer();
        remainingInputFlag = false;
    }
    
    /*
    * METHODS
    */
    
    /*
    * Precondition: N/A
    * Postcondition: returns true if the input controller has run out of file
    */
    public boolean eof() {
        return !inputController.hasNext();
    }
    
    /*
    * Precondition: N/A
    * Postcondition: returns true if eof and buffer still holds a value
    */
    public boolean remainingInput() {
        return remainingInputFlag;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: returns the next valid token, either a legal token, null, or tundf
    */    
    public Token getToken() {
        Token token = null;            //null token to return and ignore
        String nextChar;               //the next input from the InputController
        //loop through the buffer if something is left
        if (buffer.size() > 0) {
            for (int i = 0; i < buffer.size(); i++) {
                nextChar = buffer.get(i);
                machine.consume(nextChar);
                if (machine.inAccept()) {
                    token = foundToken(buffer);       //tokenize the buffer
                    buffer.clear();                   //don't need the buffer
                    machine.reset();                  //back to the start state
                    handleCursor(nextChar);
                    break;
                } else if (machine.inError()) {
                    machine.reset();                    //reset the machine
                    token = foundLexicalError(buffer);  //error routine handles everything
                    handleCursor(nextChar);
                    break;  
                }
            }
        }
        //buffer is empty, continue reading from the input file
        if (token == null) {             //if the token hasn't been assigned
            while (true) {               //currently an endless loop, breaks when appropriate
                nextChar = inputController.next();    //grab the next character
                if (!nextChar.equals("\n")) {
                    buffer.add(nextChar);  
                } 
                machine.consume(nextChar);            //statemachine consumes input
                if (eof() && !machine.inAccept() && !machine.inError() && !nextChar.contains("\n")) {                  
                    machine.consume("\n");            //force the current token into a state at eof
                    handleCursor(nextChar);           //handle the cursor 
                }
                if (machine.inAccept()) {             //the machine has accepted a token
                    token = foundToken(buffer);       //tokenize the buffer
                    buffer.clear();                   //don't need the buffer
                    machine.reset();                  //back to the start state
                    handleCursor(nextChar);
                    break;
                } else if (machine.inError()) {       //the machine has rejected a token
                    machine.reset();                    //reset the machine
                    token = foundLexicalError(buffer);  //error routine handles everything
                    handleCursor(nextChar);
                    break;                              //no need to reset the state etc.
                }
                handleCursor(nextChar);
                if(eof()) {                           //this stops the scanner from getting a null
                    break;                            //element exception on a blank input
                }                                     //and allows the last token to be tokenized
            }
        }
        return token;
    }
    
    /*
    * Precondition: a valid ArrayList as an argument
    * Postcondition: the corresponding legal token is returned, otherwise null
    */
    public Token foundToken(ArrayList<String> buffer) {
        Token token = null;
        String last = machine.lastState().getName();
        int count = 0;
       
        //whitespace removal code to assist in removing whitespace before and after valid strings
        //but not from within the string itself
        for (int i = 0; i < buffer.size(); i++) {
            if (buffer.get(i).equals("\"") && count == 0) {
                count++;
            } else if (buffer.get(i).equals("\"") && count == 1) {
                count = 0;
            } else if (count != 1 && buffer.get(i).equals(" ")) {
                buffer.remove(i);
                i--;
            }
        }
        int column = cursor.getCol() - buffer.size();
        switch (last) {
            case "Identifier":
                token = tokenizer.tokenize(buffer, cursor.getLine(), column, 0);
                break;
            case "Integer":
                token = tokenizer.tokenize(buffer, cursor.getLine(), column, 1);
                break;
            case "Real":
                token = tokenizer.tokenize(buffer, cursor.getLine(), column, 2);
                break;
            case "Operator":
            case "Double Operator":
            case "Slash":
                token = tokenizer.tokenize(buffer, cursor.getLine(), column, -1);
                break;
            case "Complete String":
                token = tokenizer.tokenize(buffer, cursor.getLine(), column, 3);
                break;
            case "Delimiter":
                token = tokenizer.tokenize(buffer, cursor.getLine(), column, -1);
                break;
            case "Comment":
                token = null;
                break;
            case "UNDF":
            case "Exclamation":
                //write the error to the program listing
                outputController.writeFile("\nLexical error: undefined token found on line: " +
                        cursor.getLine() + " column: " + column);
                token = tokenizer.tokenize(buffer, cursor.getLine(), column, 4);
            default:
                break;
        }
        return token;
    }
    
    /*
    * Precondition: a valid ArrayList as an argument
    * Postcondition: returns a valid token from illegal input, else a tundf returned
    */
    public Token foundLexicalError(ArrayList<String> buffer) {
        Token token;
        String last = machine.lastState().getName();    //the last state of machine
        ArrayList<String> tempBuffer = new ArrayList<>();   //tempBuffer used to tokenize before error
        if (last.equals("String")) {                    //a string to error is TUNDF
            int column = cursor.getCol() - (buffer.size());
            token = tokenizer.tokenize(buffer, cursor.getLine(), column, 4);
            buffer.clear();
        } else {                                        //handle the next valid token
            //remove whitespace
            if (!last.equals("Complete String")) {
                while (buffer.contains(" ")) {
                    buffer.remove(" ");
                }
            }
            tempBuffer.addAll(buffer);                  //copy buffer to temp 
            if (last.equals("Possible Real")) {         //possible real e.g. 123.a remove the dot also
                if (tempBuffer.get(tempBuffer.size() - 1).equals(".")) {
                    tempBuffer.remove(tempBuffer.size() - 1);
                } else {
                    tempBuffer.remove(tempBuffer.size() - 1); 
                    tempBuffer.remove(tempBuffer.size() - 1); 
                }
                cursor.decrementCol();
            } else if (last.equals("Dash")) {                  //a gross problem with possible comment state
                if (tempBuffer.get(tempBuffer.size() - 1).equals("-")) {
                    tempBuffer.remove(tempBuffer.size() - 1);
                } else {
                    tempBuffer.remove(tempBuffer.size() - 1); 
                    tempBuffer.remove(tempBuffer.size() - 1); 
                }
                cursor.decrementCol();
            } else {
                tempBuffer.remove(tempBuffer.size() - 1);   //remove the last item from buffer
            }
            for (int i = 0; i < tempBuffer.size(); i++) {
                machine.consume(tempBuffer.get(i));         //place machine in the correct state
                buffer.remove(tempBuffer.get(i));           //remove legal token from buffer
            }
            machine.consume("\n");                          //accept the token
            token = foundToken(tempBuffer);                 //tokenize before the error
            if (eof()) {                                    //handle a remaining token at EOF
                remainingInputFlag = true;
            }
            machine.reset();                                //state = start
        }
        return token;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: tokenize the remaining input in the buffer
    */
    public Token getRemainingToken() {
        Token token = null;
        ArrayList<String> tempBuffer = new ArrayList<>();   //tempBuffer used if an error token is in the buffer
        machine.reset();
        if (buffer.size() == 2 && (buffer.get(0).equals(".") || buffer.get(0).equals("-"))) {
            tempBuffer.add(buffer.get(0));
            buffer.remove(0);
            machine.consume(tempBuffer.get(0));
            machine.consume("\n");
            token = foundToken(tempBuffer);
            handleCursor(tempBuffer.get(0));
        } else {
            for (int i = 0; i < buffer.size(); i++) {
                machine.consume(buffer.get(i));         //move machine to the correct state
            }
            machine.consume("\n");                      //consume a state ending token
            remainingInputFlag = false;
            token = foundToken(buffer);                  //i know there's a valid token here
        }
        return token;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: tokenize the remaining input in the buffer
    */
    public Token eofToken() {
        return new Token(0, cursor.getLine(), cursor.getCol(), null);
    }
    
    /*
    * Precondition: nextChar must be a string
    * Postcondition: moves the cursor to the correct position depending on input
    */
    public void handleCursor(String nextChar) {
        if (nextChar.equals("\n")) {
            cursor.updateLine();                
        } else {                                
            cursor.updateCol();
        }
    }
    
    /*
    * Precondition: N/A
    * Postcondition: calls the OutputController to print the token
    */
    public void printToken(Token token) {
        outputController.printToken(token);
    }
    
    /*
    * Precondition: N/A
    * Postcondition: calls the OutputController to a string
    */
    public void appendProgramListing(String nextChar) {
        outputController.writeFile(nextChar);
    }
    
    /*
    * Precondition: N/A
    * Postcondition: calls the OutputController to print the token in the buffer
    */
    public void appendProgramListing(ArrayList<String> buffer) {
        for (int i = 0; i < buffer.size(); i++) {
            outputController.writeFile(buffer.get(i));
        }
    }
    
}
