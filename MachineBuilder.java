/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: MachineBuilder.java
 * Description: returns a completed state machine with required transitions
 */

import java.util.ArrayList;
import java.util.List;

public class MachineBuilder {
    //member variables
    //list of possible states
    private final State start = new State("Start");
    private final State ident = new State("Identifier");
    private final State intgr = new State("Integer");
    private final State posReal = new State("Possible Real");
    private final State real = new State("Real");
    private final State operator = new State("Operator");
    private final State doubleOp = new State("Double Operator");
    private final State delim = new State("Delimiter");
    private final State strng = new State("String");
    private final State finalStrng = new State("Complete String");
    private final State slash = new State("Slash");
    private final State dash = new State("Dash");
    private final State comment = new State("Comment");
    private final State exclamation = new State("Exclamation");
    private final State error = new State("Lexical Error");
    private final State token = new State("Legal Token");
    private final State undf = new State("UNDF");
    
    private final ConditionList conditions;
    private final List<Transition> transitions;
    private final StateMachine machine;
        
    /*
    * CONSTRUCTOR - create a state machine, initialize all transitions and 
    * assign the start, error, and accepting states
    */
    public MachineBuilder() {
        conditions = new ConditionList();
        transitions = new ArrayList<>();
        //start state transitions
        transitions.add(new Transition(start, conditions.numbers(), intgr));
        transitions.add(new Transition(start, conditions.letters(), ident));
        transitions.add(new Transition(start, conditions.operators(), operator));
        transitions.add(new Transition(start, conditions.equals(), operator));
        transitions.add(new Transition(start, conditions.delimiters(), delim));
        transitions.add(new Transition(start, conditions.quote(), strng));
        transitions.add(new Transition(start, conditions.slash(), slash));
        transitions.add(new Transition(start, conditions.dash(), delim));
        transitions.add(new Transition(start, conditions.dot(), delim));
        transitions.add(new Transition(start, conditions.excl(), exclamation));
        transitions.add(new Transition(start, conditions.illegal(), undf));
        transitions.add(new Transition(start, conditions.newline(), start));
        transitions.add(new Transition(start, conditions.space(), start));
        transitions.add(new Transition(start, conditions.tab(), start));
        
                
        //identifier states
        transitions.add(new Transition(ident, conditions.space(), token));
        transitions.add(new Transition(ident, conditions.newline(), token));
        transitions.add(new Transition(ident, conditions.tab(), token));
        transitions.add(new Transition(ident, conditions.letters(), ident));
        transitions.add(new Transition(ident, conditions.numbers(), ident));
        
        //intgr state transitions
        transitions.add(new Transition(intgr, conditions.space(), token));
        transitions.add(new Transition(intgr, conditions.newline(), token));
        transitions.add(new Transition(intgr, conditions.tab(), token));
        transitions.add(new Transition(intgr, conditions.numbers(), intgr));
        transitions.add(new Transition(intgr, conditions.dot(), posReal));
        
        //possible real state transitions
        transitions.add(new Transition(posReal, conditions.numbers(), real));
        
        //real state transitions
        transitions.add(new Transition(real, conditions.space(), token));
        transitions.add(new Transition(real, conditions.newline(), token));
        transitions.add(new Transition(real, conditions.tab(), token));
        transitions.add(new Transition(real, conditions.numbers(), real));
        
        //operator state transitions
        transitions.add(new Transition(operator, conditions.space(), token));
        transitions.add(new Transition(operator, conditions.newline(), token));
        transitions.add(new Transition(operator, conditions.tab(), token));
        transitions.add(new Transition(operator, conditions.equals(), doubleOp));
        
        //double operator state transitions
        transitions.add(new Transition(doubleOp, conditions.space(), token));
        transitions.add(new Transition(doubleOp, conditions.newline(), token));
        transitions.add(new Transition(doubleOp, conditions.tab(), token));
    
        //delimiter state transitions
        transitions.add(new Transition(delim, conditions.space(), token));
        transitions.add(new Transition(delim, conditions.newline(), token));
        transitions.add(new Transition(delim, conditions.tab(), token));
        
        //string state transitions
        transitions.add(new Transition(strng, conditions.letters(), strng));
        transitions.add(new Transition(strng, conditions.numbers(), strng));
        transitions.add(new Transition(strng, conditions.operators(), strng));
        transitions.add(new Transition(strng, conditions.delimiters(), strng));
        transitions.add(new Transition(strng, conditions.space(), strng));
        transitions.add(new Transition(strng, conditions.tab(), strng));
        transitions.add(new Transition(strng, conditions.slash(), strng));
        transitions.add(new Transition(strng, conditions.dash(), strng));
        transitions.add(new Transition(strng, conditions.dot(), strng));
        transitions.add(new Transition(strng, conditions.illegal(), strng));
        transitions.add(new Transition(strng, conditions.quote(), finalStrng));
        
        //completed string transitions
        transitions.add(new Transition(finalStrng, conditions.space(), token));
        transitions.add(new Transition(finalStrng, conditions.newline(), token));
        transitions.add(new Transition(finalStrng, conditions.tab(), token));
        
        //comment state transitions
        transitions.add(new Transition(slash, conditions.dash(), dash));
        transitions.add(new Transition(slash, conditions.newline(), token));
        transitions.add(new Transition(slash, conditions.space(), token));
        transitions.add(new Transition(slash, conditions.tab(), token));
        transitions.add(new Transition(dash, conditions.dash(), comment));
        transitions.add(new Transition(comment, conditions.letters(), comment));
        transitions.add(new Transition(comment, conditions.numbers(), comment));
        transitions.add(new Transition(comment, conditions.operators(), comment));
        transitions.add(new Transition(comment, conditions.delimiters(), comment));
        transitions.add(new Transition(comment, conditions.space(), comment));
        transitions.add(new Transition(comment, conditions.tab(), comment));
        transitions.add(new Transition(comment, conditions.slash(), comment));
        transitions.add(new Transition(comment, conditions.dash(), comment));
        transitions.add(new Transition(comment, conditions.quote(), comment));
        transitions.add(new Transition(comment, conditions.dot(), comment));
        transitions.add(new Transition(comment, conditions.illegal(), comment));
        transitions.add(new Transition(comment, conditions.newline(), token));
        
        //undefined token conditions
        transitions.add(new Transition(undf, conditions.illegal(), undf));
        //transitions.add(new Transition(undf, conditions.excl(), undf));
        transitions.add(new Transition(undf, conditions.space(), token));
        transitions.add(new Transition(undf, conditions.tab(), token));
        transitions.add(new Transition(undf, conditions.newline(), token));
        
        //excalamation token conditions
        transitions.add(new Transition(exclamation, conditions.equals(), doubleOp));
        //transitions.add(new Transition(exclamation, conditions.excl(), exclamation));
        transitions.add(new Transition(exclamation, conditions.illegal(), undf));
        transitions.add(new Transition(exclamation, conditions.space(), token));
        transitions.add(new Transition(exclamation, conditions.tab(), token));
        transitions.add(new Transition(exclamation, conditions.newline(), token));
        
        machine = new StateMachine(start, transitions, error, token);
    }
    
    public StateMachine getMachine() {
        return machine;
    }      
}
