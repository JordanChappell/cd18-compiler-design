/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: Parser.java
 * Description: Syntax/Semantic Analysis for the CD18 compiler
 */

import java.io.PrintWriter;
import java.util.ArrayList;

public class Parser {
    //member variables    
    private ArrayList<Token> tokenList;
    private static int tokenIndex;
    private Scope global;           //keeps track of global symbols
    private Scope currentScope;     //the current scope of the program
    private TreeNode root;
    private OutputController outputController;  //used to write to the program listing when an syntaxError is found
    private PrintWriter out;
    private int nodeSymbolType;
    private ArrayList<Symbol> subtreeSymbols;
    
    /*
    * CONSTRUCTOR
    */
    public Parser(ArrayList<Token> tokenList, OutputController output) {
        this.tokenList = tokenList;
        tokenIndex = 0;
        global = new Scope("global");
        currentScope = global;
        root = null;
        outputController = output;
        out = new PrintWriter(System.out, true);
        nodeSymbolType = 1;
        subtreeSymbols = new ArrayList<>();
    }
    
    /*
    * HELPER METHODS
    */
    
    /*
    * Precondition: tokenIndex can't exceed size of array list
    * Postcondition: returns the next token from the tokenList, increments counter
    */
    private Token getNextToken() {
        if (tokenIndex == tokenList.size()) {
            syntaxError("token list size exceeded, TOKEN EXPECTED");
        }
        Token token = tokenList.get(tokenIndex);
        tokenIndex++;
        return token;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: look ahead to the next token in the list, don't increment counter
    */
    private String lookahead() {
        return tokenList.get(tokenIndex).toString();
    }
    
    /*
    * Precondition: N/A
    * Postcondition: look ahead to the next token in the list, don't increment counter
    */
    private boolean sync(Token syncToken) {
        Token token = null;
        Token end = new Token(1, 0, 0, null);
        while (token != syncToken && token != end) {
            token = getNextToken();
        }
        if (token == end) {
            return false;
        } else {
            return true;
        }
    }
    
    /*
    * Precondition: argument must be a Token
    * Postcondition: instantiate a new symbol class from a given token and return        
    */
    private Symbol tokenToSymbol(Token token) {
        Symbol symbol = null;
        symbol = new Symbol(token.getStr());
        return symbol;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: instantiate a new symbol class from a given token and return           
    */
    private Symbol tokenToSymbol(Token token, int symbolType) {
        Symbol symbol = null;
        switch (symbolType) {
            case 1: 
                symbol = new Symbol(token.getStr(), 1);
                break;
            case 2:
                symbol = new Symbol(token.getStr(), 2);
                break;
            case 3:
                symbol = new Symbol(token.getStr(), 3);
                break;
            case 4:
                symbol = new StructSymbol(token.getStr());
                break;
            case 5:
                symbol = new ArraySymbol(token.getStr());
                break;
            case 6:
                symbol = new ArrayLiteral(token.getStr());
                break;
            case 7:
                symbol = new FuncSymbol(token.getStr());
            default:
                break;
        }
        return symbol;
    }
    
    /*
    * Precondition: arguments must be a Scope and Symbol
    * Postcondition: handle a STRecord for the given scope, returns true if successful
    */
    private boolean stRec(Symbol symbol) {
        if (currentScope.getTable().search(symbol) == null) {
            currentScope.getTable().addSymbol(symbol);
            return true;
        }
        return false;
    }
    
    /*
    * Precondition: arguments must be a Scope and Symbol
    * Postcondition: handle a STRecord for the given scope
    */
    private Symbol searchST(Symbol symbol) {
        Symbol s = currentScope.getTable().search(symbol);
        if (s == null) {
            s = global.getTable().search(symbol);
            //System.out.println(s);
        }
        return s;
    }
    
    private Symbol searchST(String identifier) {
        Symbol s = currentScope.getTable().search(identifier);
        if (s == null) {
            s = global.getTable().search(identifier);
        }
        return s;
    }
    
    private boolean compareSymbol(String id, int type) {
        Symbol search = searchST(id);
        if (search == null) {
            return false;
        }
        if (search.getType() == type) {
            return true;
        }
        return false;
    }
        
    /*
    * Precondition: requires a TreeNode argument
    * Postcondition: search through the partial tree, find the symbol type
    *                e.g. tree input has 2 INT types, return INT type
    *                e.g. tree input has 1 INT 1 REAL, return REAL type
    */
    private int getNodeSymbolType(TreeNode tn) {
        int temp = -1;
        if (tn.getSymbol() != null) {
            temp = tn.getType();
            
        } else if (tn.getValue() == TreeNode.NTRUE || tn.getValue() == TreeNode.NFALS) {
            temp = 3;
        }
        if (nodeSymbolType == 1) {
            if (temp == 1) {
                nodeSymbolType = 1;
            } else if (temp == 2) {
                nodeSymbolType = 2;
            } else if (temp == 3) {
                nodeSymbolType = -1;
            }
        } else if (nodeSymbolType == 2) {
            if (temp == 3) {
                nodeSymbolType = -1;
            }
        } else if (nodeSymbolType == 3) {
            if (temp != 3) {
                nodeSymbolType = -1;
            }
        } else if (nodeSymbolType == -1) {
            return nodeSymbolType;
        }
        //recurse through the tree
        if (tn.getLeft() != null) {
            nodeSymbolType = getNodeSymbolType(tn.getLeft());
        }
        if (tn.getMiddle() != null) {
            nodeSymbolType = getNodeSymbolType(tn.getMiddle());
        }
        if (tn.getRight() != null) {
            nodeSymbolType = getNodeSymbolType(tn.getRight());
        }
        return nodeSymbolType;
    }
    
    /*
    * Precondition: Argument must be a tree node
    * Postcondition: check the Symbol of the sub tree, returns true on Integer and Real
    */
    private void resetNodeSymbolType() {
        nodeSymbolType = 1;
    }
    
    
    /*
    * Precondition: Argument must be a tree node
    * Postcondition: check the Symbol of the sub tree, returns true on Integer and Real
    */
    private boolean checkIntReal(TreeNode tn) {
        if (tn.getSymbol() != null) {
            int type = tn.getType();
            if (type == 1 || type == 2) {
                return true;
            }   
        }
        if (tn.getLeft() != null) {
            return checkIntReal(tn.getLeft());
        } 
        if (tn.getMiddle() != null) {
            return checkIntReal(tn.getMiddle());
        }
        if (tn.getRight() != null) {
            return checkIntReal(tn.getRight());
        }
        return false;
    }
    
    /*
    * Precondition: Argument must be a tree node
    * Postcondition: check the Symbol of the sub tree, returns true on Integer
    */
    private boolean checkInt(TreeNode tn) {
        if (tn.getSymbol() != null) {
            int type = tn.getType();
            if (type == 1) {
                return true;
            }   
        }
        if (tn.getLeft() != null) {
            return checkInt(tn.getLeft());
        } 
        if (tn.getMiddle() != null) {
            return checkInt(tn.getMiddle());
        }
        if (tn.getRight() != null) {
            return checkInt(tn.getRight());
        }
        return false;
    }
    
    /*
    * Precondition: Argument must be a tree node
    * Postcondition: add all symbols in subtree to global arraylist and return
    */
    private ArrayList<Symbol> getSubtreeSymbols(TreeNode tn) {
        if (tn.getSymbol() != null) {
            subtreeSymbols.add(tn.getSymbol());
        }
        if (tn.getLeft() != null) {
            getSubtreeSymbols(tn.getLeft());
        }
        if (tn.getMiddle() != null) {
            getSubtreeSymbols(tn.getMiddle());
        }
        if (tn.getRight() != null) {
            getSubtreeSymbols(tn.getRight());
        }
        return subtreeSymbols;
    }
    
    
    /*
    * Precondition: argument must be a String
    * Postcondition: prints syntax error message to console, updates the program listing, 
                     exits the program.
    */
    private void syntaxError(String msg) {
        Token t = tokenList.get(tokenIndex);
        String outputError = "Syntax Error: " + msg + ", recieved " + t.toString() +
                "\nOccurred: line " + t.getLn() + " col " + t.getPos();
        System.err.println(outputError);
        System.err.println("Terminating application");
        outputController.writeError("\n" + outputError);
        outputController.writeErrorCount();
        System.out.println("\nPartial Tree output:\n");
        TreeNode.printTree(out, root);
        System.exit(0);
    }
    
    /*
    * Precondition: argument must be a String
    * Postcondition: prints semantic error message to console, updates the program listing.
    */
    private void semanticError(String msg, Token t) {
        String outputError = "Semantic Error: " + msg +
                "\nOccurred: line " + t.getLn() + " col " + t.getPos();
        System.err.println(outputError);
        outputController.writeError("\n" + outputError);
    }
    
    /*
    * GRAMMAR RULE METHODS
    */

    public TreeNode program() { 
        Token token;
        Symbol symbol;
        if (FirstSets.PROGRAM.contains(lookahead())) {
            getNextToken();
            if (lookahead().equals("TIDEN ")) {
                token = getNextToken();           //get the TIDEN token
                root = new TreeNode(TreeNode.NPROG, tokenToSymbol(token));    //create the root TN
                root.setLeft(globals());
                if (FirstSets.FUNCS.contains(lookahead())) {
                    root.setMiddle(funcs());
                }
                root.setRight(mainbody());
                if (!lookahead().equals("TEOF  ")) {
                    syntaxError("unexpected input after end of program");
                }
            } else {
                syntaxError("expected an identifier after CD18 keyword");
            }
        } else {
            syntaxError("expected CD18 keyword at start of module file");
        }
        return root;
    }
 
    private TreeNode globals() {
        TreeNode tn = new TreeNode(TreeNode.NGLOB);
        TreeNode tn2 = null;
        Token token;
        if (lookahead().equals("TCONS ")) {
            getNextToken();
            tn.setLeft(initlist());
            if (lookahead().equals("TTYPS ")) {
                getNextToken();
                tn.setMiddle(typelist());
                if (lookahead().equals("TARRS ")) {
                    getNextToken();
                    tn.setRight(arrdecls());
                }
            } else if (lookahead().equals("TARRS ")) {
                getNextToken();
                tn.setRight(arrdecls());
            }
        } else if (lookahead().equals("TTYPS ")) {
            getNextToken();
            tn.setMiddle(typelist());
            if (lookahead().equals("TARRS ")) {
                getNextToken();
                tn.setRight(arrdecls());
            }
        } else if (lookahead().equals("TARRS ")) {
            getNextToken();
            tn.setRight(arrdecls());
        }            
        return tn;
    }
   
    private TreeNode initlist() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = init();
        if (lookahead().equals("TTYPS ") || lookahead().equals("TARRS ")
                || lookahead().equals("TFUNC ") || lookahead().equals("TMAIN ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = initlist();
            return new TreeNode(TreeNode.NILIST, tn1, tn2);
        } else {
            syntaxError("unexpected token while declaring global variables");
        }
        return tn1;
    }    
  
    private TreeNode init() {
        Token token;
        TreeNode tn = null;
        if (FirstSets.INIT.contains(lookahead())) {
            token = getNextToken();
            tn = new TreeNode(TreeNode.NINIT, tokenToSymbol(token));
            //set the symbol to a constant variable
            tn.getSymbol().setConstant(true);
            if (lookahead().equals("TEQUL ")) {
                getNextToken();
                tn.setLeft(expr());
                //set the symbol type for tn.symbol to the expr type
                tn.setType(getNodeSymbolType(tn.getLeft()));
                //reset the integer flag
                resetNodeSymbolType();
                //create a record in the global symbol table
                if (!stRec(tn.getSymbol())) {
                    semanticError("Constant Declaration: global variable '" + tn.getSymbol().getIdentifier() +
                            "' is already declared", token);
                }
            } else {
                syntaxError("unexpected token during variable declaration, expected '='");
            }
        } else {
            syntaxError("unexpected token during variable declaration, expected an identifier");
        }
        return tn;
    }
    
    private TreeNode expr() {
        TreeNode tn = null;
        tn = term();
        return exprtail(tn);
    }
    
    private TreeNode exprtail(TreeNode left) {
        TreeNode parent = null, right = null;
        Token token;
        if (lookahead().equals("TPLUS ") || lookahead().equals("TMINS ")) {
            token = getNextToken();
            if (token.toString().equals("TPLUS ")) {
                parent = new TreeNode(TreeNode.NADD);
            } else if (token.toString().equals("TMINS ")) {
                parent = new TreeNode(TreeNode.NSUB);
            }
            //check left for an integer or real symbol
            if (!checkIntReal(left)) {
                semanticError("Operator: illegal operand for operator '+' or '-'", tokenList.get(tokenIndex - 1));
            }
            parent.setLeft(left);
            right = term();
            //check for an integer or real symbol
            if (!checkIntReal(right)) {
                semanticError("Operator: illegal operand for operator '+' or '-'", tokenList.get(tokenIndex - 1));
            }
            parent.setRight(right);
            return exprtail(parent);
        }
        return left;
    }
    
    private TreeNode term() {
        TreeNode tn = null;
        tn = fact();
        return termtail(tn);
    }
    
    private TreeNode termtail(TreeNode left) {
        TreeNode parent = null, right = null;
        Token token;
        if (lookahead().equals("TSTAR ") || lookahead().equals("TDIVD ")) {
            token = getNextToken();
            if (token.toString().equals("TSTAR ")) {
                parent = new TreeNode(TreeNode.NMUL);
            } else if (token.toString().equals("TDIVD ")) {
                parent = new TreeNode(TreeNode.NDIV);
            }
            //check left for an integer or real symbol
            if (!checkIntReal(left)) {
                semanticError("Operator: illegal operand for operator '*' or '/'", tokenList.get(tokenIndex - 1));
            }
            parent.setLeft(left);
            right = fact();
            //check that right is an int or real
            if (!checkIntReal(right)) {
                semanticError("Operator: illegal operand for operator '*' or '/'", tokenList.get(tokenIndex - 1));
            }
            parent.setRight(right);
            return termtail(parent);
        }
        return left;
    }
    
    private TreeNode fact() {
        TreeNode tn = null;
        tn = exponent();
        return facttail(tn);
    }
    
    private TreeNode facttail(TreeNode left) {
        TreeNode parent = null, right = null;
        Token token;
        if (lookahead().equals("TCART ")) {
            getNextToken();
            parent = new TreeNode(TreeNode.NPOW);
            //check left for an integer or real symbol
            if (!checkInt(left)) {
                semanticError("Operator: illegal operand for operator '^'", tokenList.get(tokenIndex - 1));
            }
            parent.setLeft(left); 
            right = exponent();
            //check that right is an int or real
            if (!checkInt(right)) {
                semanticError("Operator: illegal operand for operator '^'", tokenList.get(tokenIndex - 1));
            }
            parent.setRight(right);
            return facttail(parent);
        }
        return left;
    }
    
    private TreeNode exponent() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.EXPONENT.contains(lookahead())) {
            switch (lookahead()) {
                case "TIDEN ":
                    //this case has two possible rules
                    tn = var();
                    //TLPAR indicates a function call
                    if (tn.getValue() == TreeNode.NSIMV && lookahead().equals("TLPAR ")) {
                        tn = fncall(tn);
                    }
                    break;
                case "TILIT ":
                    token = getNextToken();
                    tn = new TreeNode(TreeNode.NILIT, tokenToSymbol(token, 1));
                    break;
                case "TFLIT ":
                    token = getNextToken();
                    tn = new TreeNode(TreeNode.NFLIT, tokenToSymbol(token, 2));
                    break;
                case "TTRUE ":
                    getNextToken();
                    tn = new TreeNode(TreeNode.NTRUE);
                    break;
                case "TFALS ":
                    getNextToken();
                    tn = new TreeNode(TreeNode.NFALS);
                    break;
                case "TLPAR ":
                    getNextToken();
                    tn = bool();
                    if (lookahead().equals("TRPAR ")) {
                        getNextToken();
                    } else {
                        syntaxError("unexpected token expected ')'");
                    }
                    break;
                default:
                    break;
            }
        } else {
            syntaxError("unexpected token, expected a variable or literal");
        }
        return tn;
    }
    
    private TreeNode vlist() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = var();
        if (lookahead().equals("TSEMI ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = vlist();
            return new TreeNode(TreeNode.NVLIST, tn1, tn2);
        } else {
            syntaxError("unexpected token during input statement variable list, expected ';' or ','");
        }
        return tn1;
    }    
    
    private TreeNode var() {
        TreeNode tn = null;
        if (lookahead().equals("TIDEN ")) {
            Token token = getNextToken();
            if (lookahead().equals("TLBRK ")) {
                getNextToken();
                tn = new TreeNode(TreeNode.NARRV);
                if (FirstSets.EXPR.contains(lookahead())) {
                    tn.setLeft(expr());
                    if (lookahead().equals("TRBRK ")) {
                        getNextToken();
                        if (lookahead().equals("TDOT  ")) {
                            getNextToken();
                            if (lookahead().equals("TIDEN ")) {
                                Token arrVar = getNextToken();
                                //symbol table record for arrv.id
                                //use token and arrToken as token[arrToken]
                                //check that the array literal has been declared
                                if (!compareSymbol(token.getStr(), 6)) {
                                    semanticError("Variable: array '" + token.getStr() + "' has not been declared", token);
                                }
                                //now find a match with the variable of the array literal
                                ArrayLiteral arr = (ArrayLiteral) searchST(token.getStr());
                                Symbol match = arr.getIndexValue(0, arrVar.getStr());
                                if (match == null) {
                                    semanticError("Variable: array variable '" + arrVar.getStr() + "' does not exist", arrVar);
                                }
                                tn.setSymbol(match);
                            } else {
                                syntaxError("unexpected token in variable expression, expected an identifier");
                            }
                        } else {
                            syntaxError("unexpected token in variable expression, expected '.'");
                        }
                    } else {
                        syntaxError("unexpected token in variable expression, expected ']'");
                    }
                } else {
                    syntaxError("unexpected token in variable expression, expected a variable or array literal");
                }
            } else {
                //search the symbol table for an instance of the variable
                Symbol symbol = tokenToSymbol(token);
                symbol = searchST(symbol);
                if (symbol == null) {
                    semanticError("Variable: variable '" + token.getStr() + "' has not been declared", token);
                }
                switch (symbol.getType()) {
                    case -1:
                        semanticError("Symbol Type: symbol '" + token.getStr() + "' has no valid type", token);
                        break;
                    case 4:
                        semanticError("Symbol Type: symbol '" + token.getStr() + "' is a Structure, not a variable", token);
                        break;
                    case 5:
                        semanticError("Symbol Type: symbol '" + token.getStr() + "' is an Array Declaration, not a variable", token);
                        break;
                    case 6:
                        semanticError("Symbol Type: symbol '" + token.getStr() + "' is an Array Literal, not a variable", token);
                        break;
                    default:
                        break;
                }
                tn = new TreeNode(TreeNode.NSIMV, symbol);
            }
        } else {
            syntaxError("unexpected token in variable expression, expected an identifier");
        }
        
        return tn;       
    }
    
    private TreeNode typelist() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = type();
        if (lookahead().equals("TARRS ") || lookahead().equals("TFUNC ")
                || lookahead().equals("TMAIN ")) {
            return tn1;
        }
        if (lookahead().equals("TIDEN ")) {
            tn2 = typelist();
            return new TreeNode(TreeNode.NTYPEL, tn1, tn2);
        } else {
        }
        return tn1;
    }
    
    private TreeNode type() {
        TreeNode tn = null;
        Token token;
        Token type;
        if (FirstSets.TYPE.contains(lookahead())) {
            token = getNextToken();
            if (lookahead().equals("TIS   ")) {
                getNextToken();
                if (lookahead().equals("TARAY ")) {
                    getNextToken();
                    tn = new TreeNode(TreeNode.NATYPE);
                    //assign an array declaration symbol
                    ArraySymbol symbol = (ArraySymbol) tokenToSymbol(token, 5);
                    if (lookahead().equals("TLBRK ")) {
                        getNextToken();
                        tn.setLeft(expr());
                        //check that size is an integer
                        if (getNodeSymbolType(tn.getLeft()) != 1) {
                            semanticError("Array Declaration: cannot declare non-integer array size", tokenList.get(tokenIndex - 1));
                        }
                        //reset the integer flag
                        resetNodeSymbolType();
                        //set the array size
                        symbol.setSize(1);
                        if (lookahead().equals("TRBRK ")) {
                            getNextToken();
                            if (lookahead().equals("TOF   ")) {
                                getNextToken();
                                if (lookahead().equals("TIDEN ")) {
                                    token = getNextToken();
                                    //check that the structure is valid
                                    StructSymbol struct = (StructSymbol) tokenToSymbol(token, 4);
                                    if (!compareSymbol(struct.getIdentifier(), 4)) {
                                        semanticError("Array Declaration: structure '" + struct.getIdentifier() + "' has not been declared"
                                                        , token);
                                    }
                                    symbol.setStructure((StructSymbol) searchST(struct));
                                    //set tree node symbol and record in the symbol table
                                    if (!stRec(symbol)) {
                                        semanticError("Array Declaration: array '" + symbol.getIdentifier() + "' is already declared", token);
                                    }
                                    tn.setSymbol(symbol);
                                } else {
                                    syntaxError("unexpected token in structure expression, expected an identifier");
                                }
                            } else {
                                syntaxError("unexpected token in structure expression, expected 'of' keyword");
                            }
                        } else {
                            syntaxError("unexpected token in structure expression, expected ']'");
                        }
                    } else {
                        syntaxError("unexpected token in structure expression, expected '['");
                    }
                } else {
                    StructSymbol symbol = (StructSymbol) tokenToSymbol(token, 4);
                    tn = new TreeNode(TreeNode.NRTYPE);
                    tn.setLeft(fields());    
                    //add the fields to the structure symbol
                    if (!symbol.addFields(tn.getLeft())) {
                        semanticError("Structure Declaration: structure '" + symbol.getIdentifier() + "' has a duplicate field declaration", token);
                    }
                    //record in the symbol table
                    if (!stRec(symbol)) {
                        semanticError("Structure Declaration: structure '" + symbol.getIdentifier() + "' is already declared", token);
                    }
                    //set treenode symbol
                    tn.setSymbol(symbol);
                    if (lookahead().equals("TEND  ")) {
                        getNextToken();
                    }
                }
            } else {
                syntaxError("unexpected token in structure expression, expected 'is' keyword");
            }
        } else {
            syntaxError("unexpected token in structure expression, expected an identifier");
        }
        return tn;
    }
    
    private TreeNode fields() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = sdecl();
        if (lookahead().equals("TEND  ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = fields();
            return new TreeNode(TreeNode.NFLIST, tn1, tn2);
        } else {
            syntaxError("unexpected token in structure fields list, expected 'end' keyword or ','");
        }
        return tn1;
    }    
    
   private TreeNode sdecl() {
        Token token;
        TreeNode tn = null;
        if (FirstSets.SDECL.contains(lookahead())) {
            token = getNextToken();            
            tn = new TreeNode(TreeNode.NSDECL, tokenToSymbol(token));
            if (lookahead().equals("TCOLN ")) {
                getNextToken();
                tn = stype(tn);
            } else {
                syntaxError("unexpected token during variable declaration, expected ':'");
            }
        } else {
            syntaxError("unexpected token during variable declaration, expected an identifier");
        }
        return tn;
    }
    
    private TreeNode stype(TreeNode tn) {
        Token token = getNextToken();
        switch (token.toString()) {
            case "TINTG ":
                tn.getSymbol().setType(Symbol.SINTG);
                tn.getSymbol().setValue("0");
                break;
            case "TREAL ":
                tn.getSymbol().setType(Symbol.SREAL);
                tn.getSymbol().setValue("0.0");
                break;
            case "TBOOL ":
                tn.getSymbol().setType(Symbol.SBOOL);
                tn.getSymbol().setValue("0");
                break; 
            default:
                syntaxError("unexpected token during variable declaration, expected 'integer', 'real', or 'boolean' keywords");
                break;
        }
        return tn;
    }
    
    private TreeNode arrdecls() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = arrdecl();
        if (lookahead().equals("TFUNC ") || lookahead().equals("TMAIN ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = arrdecls();
            return new TreeNode(TreeNode.NALIST, tn1, tn2);
        } else {
            syntaxError("unexpected token during array declaration list expected ','");
        }
        return tn1;
    }    
        
    private TreeNode arrdecl() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.ARRDECL.contains(lookahead())) {
            token = getNextToken();
            ArrayLiteral symbol = (ArrayLiteral) tokenToSymbol(token, 6);
            tn = new TreeNode(TreeNode.NARRD);
            if (lookahead().equals("TCOLN ")) {
                getNextToken();
                if (lookahead().equals("TIDEN ")){
                    token = getNextToken();
                    ArraySymbol arrSymbol = (ArraySymbol) tokenToSymbol(token, 5);
                    if (!compareSymbol(arrSymbol.getIdentifier(), 5)) {
                        semanticError("Array Declaration: array type '" + arrSymbol.getIdentifier() + "' has not been declared", token);
                    } else {
                        symbol.setArray((ArraySymbol) searchST(arrSymbol));
                    }
                    if (!stRec(symbol)) {
                        semanticError("Array Declaration: array literal '" + symbol.getIdentifier() + "' has already been declared", tokenList.get(tokenIndex - 1));
                    }
                    tn.setSymbol(symbol);
                }
            } else {
                syntaxError("unexpected token during array declaration, expected ':'");
            }
        } else {
            syntaxError("unexpected token during array declaration, expected an identifier");
        }
        return tn;
    }
    
    private TreeNode ambiguousdecl() {
        Token token;
        TreeNode tn = null;
        if (lookahead().equals("TIDEN ")) {
            token = getNextToken();
            tn = new TreeNode(TreeNode.NSDECL, tokenToSymbol(token));
            if (lookahead().equals("TCOLN ")) {
                getNextToken();
                if (FirstSets.AMBIGDECL.contains(lookahead())) {
                    tn = stype(tn);
                    //update the symbol table
                    if (!stRec(tn.getSymbol())) {
                        semanticError("Variable Declaration: local variable '" + token.getStr() + "' is already declared", token);
                    }
                } else if (lookahead().equals("TIDEN ")) {
                    tn.setValue(TreeNode.NARRD);
                    token = getNextToken();
                    if (!compareSymbol(token.getStr(), 5)) {
                        semanticError("Variable Declaration: array '" + token.getStr() + "' has not been declared", token);
                    }
                    ArraySymbol arrSymbol = (ArraySymbol) searchST(token.getStr());
                    tn.setSymbol(new ArrayLiteral(tn.getSymbol().getIdentifier(), arrSymbol));
                    if (!stRec(tn.getSymbol())){
                        semanticError("Variable Declaration: array variable '" + tn.getSymbol().getIdentifier() + "' is already declared", tokenList.get(tokenIndex - 1));
                    }
                } else {
                    syntaxError("unexpected token during declaration, expected an identifier or 'integer', 'real', 'boolean' keywords");
                }
            } else {
                syntaxError("unexpected token during declaration, expected ':'");
            }
        } else {
            syntaxError("unexpected token during declaration, expected an identifier");
        }
        return tn;
    }
    
    private TreeNode funcs() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = func();
        if (lookahead().equals("TMAIN ")) {
            return tn1;
        }
        if (lookahead().equals("TFUNC ")) {
            tn2 = funcs();
            return new TreeNode(TreeNode.NFUNCS, tn1, tn2);
        } else {
            syntaxError("unexpected token while parsing functions, expected 'main' or 'func' keywords");
        }
        return tn1;
    }
    
    private TreeNode func() {
        TreeNode tn = null;
        Token token;
        //change scope
        currentScope = global;
        if (lookahead().equals("TFUNC ")) {
            getNextToken();
            if (lookahead().equals("TIDEN ")) {
                token = getNextToken();                 //save the next token
                FuncSymbol symbol = (FuncSymbol) tokenToSymbol(token, 7);   //convert to a symbol
                tn = new TreeNode(TreeNode.NFUND, symbol);      //create a tree node
                //change the scope
                Scope func = new Scope(symbol.getIdentifier());
                global.addBelow(func);
                if (lookahead().equals("TLPAR ")) {
                    getNextToken();
                    if (FirstSets.PARAMS.contains(lookahead())) {
                        currentScope = func;    //change scope to functiuon for params
                        tn.setLeft(plist());
                        //add the arguments to symbol, check for duplicates
                        if (!symbol.addArguments(tn.getLeft())) {
                            semanticError("Function Declaration: function '" + symbol.getIdentifier() + "' has a duplicate parameter", token);
                        }
                        currentScope = global; //back to global for return type and function symbol
                    }
                    if (lookahead().equals("TRPAR ")) {
                        getNextToken();
                    } else {
                        syntaxError("unexpected token during function declaration, expected ')'");
                    }
                    if (lookahead().equals("TCOLN ")) {
                        getNextToken();
                        rtype(tn);
                        symbol.setType(tn.getType());
                        //record in global symbol table and set tree node symbol
                        if(!stRec(symbol)) {
                            semanticError("Function Declaration: function name has already been declared", token);
                        }
                        tn.setSymbol(symbol);
                        //change to function scope for funcbody()
                        currentScope = func;                       
                        tn = funcbody(tn);  
                    } else {
                        syntaxError("unexpected token during function declaration, expected ':'");
                    }
                } else {
                    syntaxError("unexpected token during function declaration, expected '('");
                }
            } else {
                syntaxError("unexpected token during function declaration, expected an identifier");
            }
        } else {
            syntaxError("unexpected token during function declaration, expected 'func' keyword");
        }
        return tn;
    }
    
    private TreeNode plist() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.PARAMS.contains(lookahead())) {
            tn = params();
        }         
        return tn;
    }
    
    private TreeNode params() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = param();
        if (lookahead().equals("TRPAR ")) {
            return tn1;
        } else if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = params();
            return new TreeNode(TreeNode.NPLIST, tn1, tn2);
        }
        return tn1;
    }
    
    private TreeNode param() {
        TreeNode tn1 = null, tn2 = null;
        if (lookahead().equals("TIDEN ")) {
            tn1 = ambiguousdecl();
            if (tn1.getValue() == TreeNode.NSDECL) {
                tn2 = new TreeNode(TreeNode.NSIMP);
                tn2.setLeft(tn1);
            } else {
                tn2 = new TreeNode(TreeNode.NARRP);
                tn2.setLeft(tn1);
            }
            return tn2;
        } else if (lookahead().equals("TCNST ")) {
            getNextToken();
            tn1 = new TreeNode(TreeNode.NARRC);
            tn1.setLeft(arrdecl());
        } else {
            syntaxError("unexpected token in function parameters, expected identifier or 'const' keyword");
        }
        return tn1;
    }
    
    private TreeNode rtype(TreeNode tn) {
        Token token;
        if (lookahead().equals("TVOID ")) {
            getNextToken();
            tn.getSymbol().setType(0);
        } else if (FirstSets.STYPE.contains(lookahead())) {
            tn = stype(tn);
        } else {
            syntaxError("unexpected token in function return type expected 'void', 'integer', 'real', or 'boolean' keywords");
        }
        return tn;
    }
    
    private TreeNode funcbody(TreeNode func) {
        TreeNode middle = null, right = null;
        middle = locals();
        if (lookahead().equals("TBEGN ")) {
            getNextToken();
        } else {
            syntaxError("unexpected token in function body definition, expected 'begin' keyword");
        }
        right = stats();
        if (lookahead().equals("TEND  ")) {
            Token token = getNextToken();
            //check for a return statement in the function
            FuncSymbol function = (FuncSymbol) global.getTable().search(currentScope.getName());
            //semantic error if no return statement exists
            if (!function.getReturnFlag()) {
                semanticError("Function Body: function '" + function.getIdentifier() + 
                        "' has no return statement", token);
            }
        } else {
            syntaxError("unexpected token in function body definition, expected 'end' keyword");
        }
        func.setMiddle(middle);
        func.setRight(right);
        return func;
    }
    
    private TreeNode locals() {
        TreeNode tn = null;
        Token token;
        if (lookahead().equals("TIDEN ")) {
            tn = dlist();
        } 
        if (lookahead().equals("TBEGN ")) {
            return tn;
        } else {
            syntaxError("unexpected token in local variable list, expected an identifier or 'begin' keyword");
        }
        return tn;
    }
    
    private TreeNode dlist() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = decl();
        if (lookahead().equals("TBEGN ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = dlist();
            return new TreeNode(TreeNode.NDLIST, tn1, tn2);
        } else {
            syntaxError("unexpected token in declaration list, expected 'begin' keyword or ','");
        }
        return tn1;
    }
    
    private TreeNode decl() {
        TreeNode tn = null;
        if (lookahead().equals("TIDEN ")) {
            tn = ambiguousdecl();
        } else {
            syntaxError("unexpected token during variable declaration, expected an identifier");
        }
        return tn;
    }
        
    private TreeNode stats() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        if (FirstSets.STAT.contains(lookahead())) {
            tn1 = stat();
            if (lookahead().equals("TSEMI ")) {
                getNextToken();
            } else {
                syntaxError("unexpected token after a statement expected ';'");
            }
        } else if (FirstSets.STRSTAT.contains(lookahead())) {
            tn1 = strstat();
        } else {
            syntaxError("unexpected token where a statement keyword should be");
        }
        if (FirstSets.STAT.contains(lookahead()) || FirstSets.STRSTAT.contains(lookahead())) {
            tn2 = stats();
            return new TreeNode(TreeNode.NSTATS, tn1, tn2);
        }
        return tn1;
    }
    
    private TreeNode stat() {
        TreeNode tn = null;
        if (FirstSets.REPTSTAT.contains(lookahead())) {
            tn = repstat();
        } else if (FirstSets.IOSTAT.contains(lookahead())) {
            tn = iostat();
        } else if (FirstSets.RETURNSTAT.contains(lookahead())) {
            tn = returnstat();
        } else if (lookahead().equals("TIDEN ")) {
            tn = asgnstat();
        } 
        return tn;
    }
    
    private TreeNode strstat() {
        TreeNode tn = null;
        if (FirstSets.FORSTAT.contains(lookahead())) {
            tn = forstat();
        } else if (FirstSets.IFSTAT.contains(lookahead())) {
            tn = ifstat();
        }
        return tn;
    }
    
    private TreeNode repstat() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.REPTSTAT.contains(lookahead())) {            
            getNextToken();
            tn = new TreeNode(TreeNode.NREPT);
            if (lookahead().equals("TLPAR ")) {
                getNextToken();
                tn.setLeft(asgnlist());
                if (lookahead().equals("TRPAR ")) {
                    getNextToken();
                    tn.setMiddle(stats());
                    if (lookahead().equals("TUNTL ")) {
                        getNextToken();
                        tn.setRight(bool());
                    } else {
                        syntaxError("unexpected token in repeat statement, expected 'until' keyword");
                    }
                } else {
                    syntaxError("unexpected token in repeat statement, expected ')'");
                }
            } else {
                syntaxError("unexpected token in repeat statement, expected '('");
            }
        } else {
            syntaxError("unexpected token in repeat statement, expected 'repeat' keyword");
        }
        return tn;
    }
    
    private TreeNode asgnlist() {
        TreeNode tn = null;
        if (FirstSets.ALIST.contains(lookahead())) {
            tn = alist();
        }
        return tn;
    }
    
    private TreeNode alist() {
        TreeNode tn1 = null, tn2 = null;
        tn1 = asgnstat();
        if (lookahead().equals("TRPAR ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            return new TreeNode(TreeNode.NASGNS, tn1, tn2);
        }
        return tn1;
    }
    
    private TreeNode asgnstat() {
        TreeNode left = var();
        if (lookahead().equals("TLPAR ")) {
            left = callstat(left);
        } else {
            TreeNode parent = asgnop();
            parent.setLeft(left);
            parent.setMiddle(bool());
            //semantic checking for type matching
            int leftHand = getNodeSymbolType(parent.getLeft());
            //reset the integer flag
            resetNodeSymbolType();
            int rightHand = getNodeSymbolType(parent.getMiddle());
            //reset the integer flag
            resetNodeSymbolType();
            if (leftHand != rightHand) {
                semanticError("Variable Assignment: variable '" + left.getSymbol().getIdentifier() + "'s type does not match assigned type", tokenList.get(tokenIndex - 1));
            }
            return parent;
        }
        return left;
    }
    
    private TreeNode asgnop() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.ASGNOP.contains(lookahead())) {
            token = getNextToken();
            switch (token.toString()) {
                case "TEQUL ":
                    tn = new TreeNode(TreeNode.NASGN);
                    break;
                case "TPLEQ ":
                    tn = new TreeNode(TreeNode.NPLEQ);
                    break;
                case "TMNEQ ":
                    tn = new TreeNode(TreeNode.NMNEQ);
                    break;
                case "TSTEQ ":
                    tn = new TreeNode(TreeNode.NSTEQ);
                    break;
                case "TDVEQ ":
                    tn = new TreeNode(TreeNode.NDVEQ);
                    break;
                default:
                    break;
            }
        } else {
            syntaxError("unexpected token in assignment operation, expected an assign operator");
        }
        return tn;
    }
    
     private TreeNode bool() {
        TreeNode tn = null;
        tn = rel();
        return booltail(tn);
    }
    
    private TreeNode booltail(TreeNode left) {
        TreeNode parent = null, right = null;
        Token token;
        if (FirstSets.LOGOP.contains(lookahead())) {
            parent = logop();
            parent.setLeft(left);
            right = rel();
            parent.setRight(right);
            return booltail(parent);
        }
        return left;
    }
    
    private TreeNode rel() {
        TreeNode parent = null, left = null, right = null;
        if (FirstSets.REL.contains(lookahead())) {
            if (lookahead().equals("TNOT  ")) {
                getNextToken();
                parent = new TreeNode(TreeNode.NNOT);
                parent.setLeft(expr());
                parent.setMiddle(relop());
                parent.setRight(expr());
            } else {
                left = expr();
                if (FirstSets.RELOP.contains(lookahead())) {
                    parent = relop();
                    right = expr();
                    parent.setLeft(left);
                    parent.setMiddle(right);
                } else {
                    return left;
                }    
            }
        } else {
            syntaxError("unexpected token in relation operation, expected 'not' keyword or an expression");
        }
        return parent;
    }
    
    private TreeNode logop() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.LOGOP.contains(lookahead())) {
            token = getNextToken();
            switch (token.toString()) {
                case "TAND  ":
                    tn = new TreeNode(TreeNode.NAND);
                    break;
                case "TOR   ":
                    tn = new TreeNode(TreeNode.NOR);
                    break;
                case "TXOR  ":
                    tn = new TreeNode(TreeNode.NXOR);
                    break;
                default:
                    break;
            }
        } else {
            syntaxError("unexpected token in logical operation, expected a logical operator keyword");
        }
        return tn;
    }
    
    private TreeNode relop() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.RELOP.contains(lookahead())) {
            token = getNextToken();
            switch (token.toString()) {
                case "TEQEQ ":
                    tn = new TreeNode(TreeNode.NEQL);
                    break;
                case "TNEQL ":
                    tn = new TreeNode(TreeNode.NNEQ);
                    break;
                case "TGRTR ":
                    tn = new TreeNode(TreeNode.NGRT);
                    break;
                case "TLESS ":
                    tn = new TreeNode(TreeNode.NLSS);
                    break;
                case "TLEQL ":
                    tn = new TreeNode(TreeNode.NLEQ);
                    break;
                case "TGEQL ":
                    tn = new TreeNode(TreeNode.NGEQ);
                    break;
                default:
                    break;
            }
        } else {
            syntaxError("unexpected token in relation operation, expected a relation operator");
        }
        return tn;
    }
    
    private TreeNode iostat() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.IOSTAT.contains(lookahead())) {
            token = getNextToken();
            switch (token.toString()) {
                case "TINPT ":
                    tn = new TreeNode(TreeNode.NINPUT);
                    tn.setLeft(vlist());
                    break;
                case "TPRIN ":
                    tn = new TreeNode(TreeNode.NPRINT);
                    tn.setLeft(prlist());
                    break;
                case "TPRLN ":
                    tn = new TreeNode(TreeNode.NPRLN);
                    tn.setLeft(prlist());
                    break;
                default:
                    break;
            }
        } else {
            syntaxError("unexpected token in io statement, expected 'input', 'print', or 'printline' keywords");
        }
        return tn;
    }
    
    private TreeNode fncall(TreeNode var) {
        TreeNode tn = new TreeNode(TreeNode.NFCALL, var.getSymbol());
        FuncSymbol fnSymbol = null;
        //make sure that tn has a symbol
        if (tn.getSymbol() != null) {
            //search for the function symbol
            Symbol symbol = searchST(tn.getSymbol());
            //check that a function symbol exists
            if (symbol == null || !(symbol instanceof FuncSymbol)) {
                semanticError("Function Call: function '" + symbol.getIdentifier() + "' has not been declared", tokenList.get(tokenIndex - 1));
            } else {
                //cast to a function symbol
                fnSymbol = (FuncSymbol) symbol;
            }
        }
        if (lookahead().equals("TLPAR ")) {
            getNextToken();
            if (FirstSets.BOOL.contains(lookahead())) {
                tn.setLeft(elist());
            }
            if (lookahead().equals("TRPAR ")) {
                getNextToken();
            } else {
                syntaxError("unexpected token during a function call, expected some parameters or ')'");
            }
        } else {
            syntaxError("unexpected token during a function call, expected '('");
        }
        //test subtree elist against function definition
        if (fnSymbol != null) {
            ArrayList<Symbol> args = fnSymbol.getArguments();
            ArrayList<Symbol> compare = new ArrayList<>();
            if (tn.getLeft() != null) {
                compare = (ArrayList) getSubtreeSymbols(tn.getLeft()).clone();
                subtreeSymbols.clear();     //delete the symbols from global list
            }
            //test size and symbol types
            boolean argFlag = false;
            if (args.size() != compare.size()) {
                argFlag = true;         //keep track of a failed test
            } else {
                //loop over the definition arguments and compare to given args
                for (int i = 0; i < args.size(); i++) {
                    if (args.get(i).getType() != compare.get(i).getType()) {
                        argFlag = true;
                    }
                }
            }
            if (argFlag) {
                semanticError("Function Call: given function arguments for '" + fnSymbol.getIdentifier() +
                        "' do not match formal definition", tokenList.get(tokenIndex - 1));
            }
        }
        return tn;
    }
    
    private TreeNode prlist() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = printitem();
        if (lookahead().equals("TSEMI ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = prlist();
            return new TreeNode(TreeNode.NPRLST, tn1, tn2);
        } else {
            syntaxError("unexpected token in print list, expected ';' or ','");
        }
        return tn1;
    }    
  
    private TreeNode printitem() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.PRINTITEM.contains(lookahead())) {
            if (FirstSets.EXPR.contains(lookahead())) {
                tn = expr();
            } else if (lookahead().equals("TSTRG ")) {
                token = getNextToken();
                tn = new TreeNode(TreeNode.NSTRG, tokenToSymbol(token));
            }
        } else {
            syntaxError("unexpected token, expected an expression or string");
        }
        return tn;
    }
    
    private TreeNode callstat(TreeNode var) {
        TreeNode tn = new TreeNode(TreeNode.NCALL, var.getSymbol());
        FuncSymbol fnSymbol = null;
        //make sure that tn has a symbol
        if (tn.getSymbol() != null) {
            //search for the function symbol
            Symbol symbol = searchST(tn.getSymbol());
            //check that a function symbol exists
            if (symbol == null || !(symbol instanceof FuncSymbol)) {
                semanticError("Function Call: function '" + symbol.getIdentifier() + "' has not been declared", tokenList.get(tokenIndex - 1));
            } else {
                //cast to a function symbol
                fnSymbol = (FuncSymbol) symbol;
            }
        }
        if (lookahead().equals("TLPAR ")) {
            getNextToken();
            if (FirstSets.BOOL.contains(lookahead())) {
                tn.setLeft(elist());
            }
            if (lookahead().equals("TRPAR ")) {
                getNextToken();
            } else {
                syntaxError("unexpected token during a function call, expected some parameters or ')'");
            }
        } else {
            syntaxError("unexpected token during a function call, expected '('");
        }
        //test subtree elist against function definition
        if (fnSymbol != null) {
            ArrayList<Symbol> args = fnSymbol.getArguments();
            ArrayList<Symbol> compare = new ArrayList<>();
            if (tn.getLeft() != null) {
                compare = (ArrayList) getSubtreeSymbols(tn.getLeft()).clone();
                subtreeSymbols.clear();     //delete the symbols from global list
            }
            //test size and symbol types
            boolean argFlag = false;
            if (args.size() != compare.size()) {
                argFlag = true;         //keep track of a failed test
            } else {
                //loop over the definition arguments and compare to given args
                for (int i = 0; i < args.size(); i++) {
                    if (args.get(i).getType() != compare.get(i).getType()) {
                        argFlag = true;
                    }
                }
            }
            if (argFlag) {
                semanticError("Function Call: given function arguments for '" + fnSymbol.getIdentifier() +
                        "' do not match formal definition", tokenList.get(tokenIndex - 1));
            }
        }
        return tn;
    }
    
    private TreeNode elist() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = bool();
        if (lookahead().equals("TRPAR ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = elist();
            return new TreeNode(TreeNode.NEXPL, tn1, tn2);
        } else {
            syntaxError("unexpected token in parameter list, expected ')' or ','");
        }
        return tn1;
    }    
    
    private TreeNode returnstat() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.RETURNSTAT.contains(lookahead())) {
            token = getNextToken();
            tn = new TreeNode(TreeNode.NRETN);
            if (FirstSets.EXPR.contains(lookahead())) {
                tn.setLeft(expr());
                if (currentScope.getName() != "main") {
                    FuncSymbol symbol = (FuncSymbol) global.getTable().search(currentScope.getName());
                    if (symbol != null) {
                        int returnType = getNodeSymbolType(tn.getLeft());
                        //reset the integer flag
                        resetNodeSymbolType();
                        //semantic check for return type
                        if (returnType != symbol.getType()) {
                            semanticError("Function Body: incorrect return type for function '" + symbol.getIdentifier() + "'", token);
                        }
                        //set the return flag for the function
                        symbol.setReturnFlag();
                        //update the symbol table
                        global.getTable().addSymbol(symbol);
                    }
                }
            } else {
                if (currentScope.getName() != "main") {
                    FuncSymbol symbol = (FuncSymbol) global.getTable().search(currentScope.getName());
                    if (symbol != null) {
                        if (symbol.getType() != Symbol.SVOID) {
                            semanticError("Function Body: incorrect return type for function '" + symbol.getIdentifier() + "'", token);
                        }
                        //set the return flag for the function
                        symbol.setReturnFlag();
                        //update the symbol table
                        global.getTable().addSymbol(symbol);
                    }
                }
            }
        }
        return tn;
    }
    
    private TreeNode forstat() {
        TreeNode tn = null;
        Token token;
        if (FirstSets.FORSTAT.contains(lookahead())) {
            getNextToken();
            tn = new TreeNode(TreeNode.NFOR);
            if (lookahead().equals("TLPAR ")) {
                getNextToken();
                if (FirstSets.ALIST.contains(lookahead())) {
                    tn.setLeft(asgnlist());
                }
                if (lookahead().equals("TSEMI ")) {
                    getNextToken();
                } else {
                    syntaxError("unexpected token during for-loop statement, expected ';'");
                }
                if (FirstSets.BOOL.contains(lookahead())) {
                    tn.setMiddle(bool());
                } else {
                    syntaxError("unexpected token during for-loop statement, expected a boolean comparison");
                }
                if (lookahead().equals("TRPAR ")) {
                    getNextToken();
                } else {
                    syntaxError("unexpected token during for-loop statement, expected ')'");
                }
                if (FirstSets.STAT.contains(lookahead()) || FirstSets.STRSTAT.contains(lookahead())) {
                    tn.setRight(stats());
                } else {
                    syntaxError("unexpected token during for-loop statement, expected some statements");
                }
                if (lookahead().equals("TEND  ")) {
                    getNextToken();
                } else {
                    syntaxError("unexpected token during for-loop statement, expected 'end' keyword");
                }
            } else {
                syntaxError("unexpected token during for-loop statement, expected '('");
            }
        }
        return tn;
    }
    
    private TreeNode ifstat() {
        TreeNode parent = null, left = null, middle = null;
        Token token;
        if (FirstSets.IFSTAT.contains(lookahead())) {
            getNextToken();
            if (lookahead().equals("TLPAR ")) {
                getNextToken();
                if (FirstSets.BOOL.contains(lookahead())) {
                    left = bool();
                } else {
                    syntaxError("unexpected token in if statement, expected a boolean comparison");
                }
                if (lookahead().equals("TRPAR ")) {
                    getNextToken();
                } else {
                    syntaxError("unexpected token in if statement, expected ')'");
                }
                if (FirstSets.STAT.contains(lookahead()) || FirstSets.STRSTAT.contains(lookahead())) {
                    middle = stats();
                } else {
                    syntaxError("unexpected token in if statement, expected statements to run");
                }
                if (lookahead().equals("TEND  ")) {
                    getNextToken();
                    parent = new TreeNode(TreeNode.NIFTH, left, middle);
                    return parent;
                } else if (lookahead().equals("TELSE ")) {
                    getNextToken();
                    parent = new TreeNode(TreeNode.NIFTE, left, middle);
                } else {
                    syntaxError("unexpected token in if statement, expected 'end' or 'else'");
                }
                if (FirstSets.STAT.contains(lookahead())) {
                    parent.setRight(stats());
                } else {
                    syntaxError("unexpected token in if-else statement, expected statements to execute");
                }
                if (lookahead().equals("TEND  ")) {
                    getNextToken();
                    return parent;
                } else {
                    syntaxError("unexpected token in if-else statement, expected 'end' keyword");
                }
            } else {
                syntaxError("unexpected token in if-else statement, expected '('");
            }
        }
        return parent;
    }

    private TreeNode mainbody() {
        TreeNode tn = null;
        Token token;
        Symbol symbol;
        //update scope
        Scope main = new Scope("main");
        global.addBelow(main);
        currentScope = main;
        if (lookahead().equals("TMAIN ")) {
            getNextToken();
            tn = new TreeNode(TreeNode.NMAIN);
        } else {
            syntaxError("unexpected token in main body definition, expected 'main' keyword");
        }
        tn.setLeft(slist());
        if (lookahead().equals("TBEGN ")) {
            getNextToken();
            tn.setMiddle(stats());
        } else {
            syntaxError("unexpected token in main body definition, expected 'begin' keyword");
        }
        if (lookahead().equals("TEND  ")) {
            getNextToken();
        } else {
            syntaxError("unexpected token in main body definition, expected 'end' keyword");
        }
        if (lookahead().equals("TCD18 ")) {
            getNextToken();
        } else {
            syntaxError("unexpected token in main body definition, expected 'CD18' keyword");
        }
        if (lookahead().equals("TIDEN ")) {
            token = getNextToken();         //get the TIDEN token
            symbol = tokenToSymbol(token);  //convert to symbol
            //check the symbol matches the program identifier
            if (!root.getSymbol().getIdentifier().equals(symbol.getIdentifier())) {
                semanticError("program identifier at EOF does not match", token);
            }
        } else {
            syntaxError("unexpected token in main body definition, expected an identifier");
        }
        return tn;
    }
    
    private TreeNode slist() {
        TreeNode tn1 = null, tn2 = null;
        Token token;
        tn1 = sdecl();
        //complete a symbol table record for the variable
        stRec(tn1.getSymbol());
        if (lookahead().equals("TBEGN ")) {
            return tn1;
        }
        if (lookahead().equals("TCOMA ")) {
            getNextToken();
            tn2 = slist();
            return new TreeNode(TreeNode.NSDLST, tn1, tn2);
        } else {
            syntaxError("unexpected token in main variable declarations, expected 'begin' keyword or ','");
        }
        return tn1;
    }
}
