/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: Transition.java
 * Description: A single transition in a StateMachine transition function.
 */

import java.util.List;

public class Transition {
    //member variables
    private final State from;
    private final State to;
    private final List<String> conditions;
    
    /*
    * CONSTRUCTOR
    */
    public Transition(State from, List<String> conditions, State to) {
        this.from = from;
        this.to = to;
        this.conditions = conditions;
    }
    
    public State getFrom() {
        return from;
    }
    
    public State getTo() {
        return to;
    }
    
    /*
    * Precondition: conditions must be initialised
    * Postcondition: return the list of conditions for the current transition
    */
    public List<String> getConditions() {
        return conditions;
    }
 }
