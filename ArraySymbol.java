/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: ArraySymbol.java
 * Description: an ArraySymbol extends the Symbol class to initialize a CD18 array
 */

import java.util.ArrayList;

public class ArraySymbol extends Symbol {
    //member variables
    private StructSymbol structure;     //the CD18 structure that this array holds
    private int size;                   //length of the array
    
    /*
    * CONSTRUCTORS
    */
    public ArraySymbol() {
        super();
        size = 0;
        super.setType(Symbol.SARRAY);
    }
     
    public ArraySymbol(String identifier) {
        super(identifier);
        size = 0;
        super.setType(Symbol.SARRAY);
    }
    
    public ArraySymbol(String identifier, int size) {
        super(identifier);
        this.size = size;
        super.setType(Symbol.SARRAY);
    }
        
    /*
    * SUBCLASS METHODS
    */
    
    public void setSize(int size) {
        this.size = size;
    }
    
    public int getSize() {
        return size;
    }
    
    public void setStructure(StructSymbol structure) {
        this.structure = structure;
    }
    
    public StructSymbol getStructure() {
        return structure;
    }
}
