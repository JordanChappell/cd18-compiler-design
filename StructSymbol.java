/*
 * Course: 
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: StructSymbol.java
 * Description: extends the Symbol class for a Structure in CD18
 */

import java.util.ArrayList;

public class StructSymbol extends Symbol {
    //member variables
    private ArrayList<Symbol> fields;
    private boolean errorFlag;
    
    /*
    * CONSTRUCTORS
    */
    public StructSymbol() {
        super();
        super.setType(Symbol.SSTRCT);
        fields = new ArrayList<>();
        errorFlag = true;
    }
     
    public StructSymbol(String identifier) {
        super(identifier);
        super.setType(Symbol.SSTRCT);
        fields = new ArrayList<>();
        errorFlag = true;
    }
        
    /*
    * SUBCLASS METHODS
    */
    public ArrayList<Symbol> getFields() {
        return fields;
    }
    
    public void addField(Symbol field) {
        fields.add(field);
    }
    
    public void addField(String identifier, int type) {
        fields.add(new Symbol(identifier, type));
    }
    
    /*
    * Precondition: argument must be a treenode
    * Postcondition: adds fields to the structure from the given partial tree
    */
    public boolean addFields(TreeNode tn) {
        if (fields.contains((Symbol) tn.getSymbol())) {
            errorFlag = false;
        }
        if (tn.getValue() == TreeNode.NSDECL) {
            fields.add(tn.getSymbol());
        }
        if (tn.getLeft() != null) {
            addFields(tn.getLeft());
        }
        if (tn.getMiddle() != null) {
            addFields(tn.getMiddle());
        }
        if (tn.getRight() != null) {
            addFields(tn.getRight());
        }
        return errorFlag;
    }
}
