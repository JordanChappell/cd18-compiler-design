/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: FirstSets.java
 * Description: STATIC non instantiatable class, used to retrieve required first
 *              sets for the parser.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class FirstSets {
    public static final List<String> PROGRAM = Arrays.asList("TCD18 "); 
    public static final List<String> GLOBALS = Arrays.asList("TCONS ", "TTYPS ", "TARRS ");
    public static final List<String> FUNCS = Arrays.asList("TFUNC ");
    public static final List<String> MAIN = Arrays.asList("TMAIN ");
    
    public static final List<String> INIT = Arrays.asList("TIDEN ");
    public static final List<String> TYPE = Arrays.asList("TIDEN ");
    public static final List<String> SDECL = Arrays.asList("TIDEN ");
    public static final List<String> ARRDECL = Arrays.asList("TIDEN ");
    public static final List<String> AMBIGDECL = Arrays.asList("TINTG ", "TREAL ", "TBOOL ");
    public static final List<String> EXPONENT = Arrays.asList("TIDEN ", "TILIT ",
            "TFLIT ", "TTRUE ", "TFALS ", "TLPAR ");
    public static final List<String> EXPR = Arrays.asList("TIDEN ", "TILIT ",
            "TFLIT ", "TTRUE ", "TFALS ", "TLPAR ");
    public static final List<String> STYPE = Arrays.asList("TINTG ", "TREAL ", "TBOOL ");
    
    public static final List<String> PARAMS = Arrays.asList("TCNST ", "TIDEN ");
    
    public static final List<String> REPTSTAT = Arrays.asList("TREPT ");
    public static final List<String> ASGNSTAT = Arrays.asList("TIDEN ");
    public static final List<String> IOSTAT = Arrays.asList("TINPT ", "TPRIN ", "TPRLN ");
    public static final List<String> RETURNSTAT = Arrays.asList("TRETN ");
    public static final List<String> FORSTAT = Arrays.asList("TFOR  ");
    public static final List<String> IFSTAT = Arrays.asList("TIFTH ");
    public static final List<String> STAT = Arrays.asList("TREPT ", "TIDEN ",
            "TINPT ", "TPRIN ", "TPRLN ", "TRETN ");
    public static final List<String> STRSTAT = Arrays.asList("TFOR  ", "TIFTH ");

    public static final List<String> REL = Arrays.asList("TNOT  ", "TIDEN ",
            "TILIT ", "TFLIT ", "TTRUE ", "TFALS ", "TLPAR ");
    public static final List<String> ASGNOP = Arrays.asList("TEQUL ", "TPLEQ ",
            "TMNEQ ", "TSTEQ ", "TDVEQ ");
    public static final List<String> LOGOP = Arrays.asList("TAND  ", "TOR   ", "TXOR  ");
    public static final List<String> RELOP = Arrays.asList("TEQEQ ", "TNEQL ", "TGRTR ",
            "TLESS ", "TLEQL ", "TGEQL ");
    public static final List<String> ALIST = Arrays.asList("TIDEN ");
    public static final List<String> BOOL = Arrays.asList("TNOT  ", "TIDEN ", "TILIT ",
            "TFLIT ", "TTRUE ", "TFALS ", "TLPAR ");
    
    public static final List<String> PRINTITEM = Arrays.asList("TIDEN ", "TILIT ",
            "TFLIT ", "TTRUE ", "TFALS ", "TLPAR ", "TSTRG ");
    
    /*
    * CONSTRUCTOR - Throws an exception if the class is instantiated
    */
    private FirstSets() {
        //don't allow instantiation
        throw new AssertionError();
    }
}
