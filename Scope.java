/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: Scope.java
 * Description: Scope is used in Semantic analysis to determine variable lifecycle
 */

import java.util.ArrayList;

public class Scope {
    //member variables
    private final String scopeName;     //the name of the scope
    private final ArrayList<Scope> below;          //holds the scope that is below this one
    private final SymbolTable st;       //the symbol table for this scope

    /*
    * CONSTRUCTOR
    */ 
    public Scope(String scopeName) {
        this.scopeName = scopeName;
        this.below = new ArrayList<>();
        st = new SymbolTable();
    }
    
    /*
    * METHODS - getters mostly
    */
    
    public String getName() {
        return scopeName;
    }
    
    public SymbolTable getTable() {
        return st;
    }
    
    public Scope searchBelow(Scope scope) {
        for (Scope s : below) {
            if (s.getName().equals(scope.getName())) {
                return s;
            }
        }
        return null;
    }
    
    public Scope searchBelow(String id) {
        for (Scope s : below) {
            if (s.getName().equals(id)) {
                return s;
            }
        }
        return null;
    }
    
    public boolean addBelow(Scope scope) {
        if (below.add(scope)) {
            return true;
        }
        return false;
    }
}
