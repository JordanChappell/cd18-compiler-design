/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: Cursor.java
 * Description: holds the line number and column number when reading an input file
 */

public class Cursor {
    //member variables
    private int lineNum;
    private int colNum;
    
    /*
    * CONSTRUCTOR
    */
    public Cursor() {
        lineNum = 1;
        colNum = 1;
    }
    
    /*
    * METHODS
    */
    
    public int getLine() {
        return lineNum;
    }
    
    public int getCol() {
        return colNum;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: increment line number, reset column count
    */
    public void updateLine() {
        colNum = 1;
        lineNum ++;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: increment column number
    */
    public void updateCol() {
        colNum ++;
    }
    
    /*
    * Precondition: N/A
    * Postcondition: decrement column number
    */
    public void decrementCol() {
        colNum --;
    }
}
