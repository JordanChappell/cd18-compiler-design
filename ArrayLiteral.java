/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: ArrayLiteral.java
 * Description: extends the Symbol class to implement an Array Literal in CD18, holds variables that are Structures.
 */

import java.util.ArrayList;

public class ArrayLiteral extends Symbol {
    //member variables
    private ArraySymbol array;          //reference to the array initialization
    private StructSymbol[] variables;   //Structures make up this array
    
    /*
    * CONSTRUCTORS
    */
    public ArrayLiteral() {
        super();
        array = null;
        variables = null;
        super.setType(Symbol.SARLIT);
    }
    
    public ArrayLiteral(String identifier) {
        super(identifier);
        array = null;
        variables = null;
        super.setType(Symbol.SARLIT);
    }
    
    public ArrayLiteral(String identifier, ArraySymbol array) {
        super(identifier);
        this.array = array;
        variables = new StructSymbol[array.getSize()];
        //initialize the indicies of the array as Structures
        for (int i = 0; i < variables.length; i++) {
            variables[i] = array.getStructure();
        }
        super.setType(Symbol.SARLIT);
    }
    
    /*
    * SUBCLASS METHODS
    */
    
    /*
    * Precondition: arguments must be an int and a String.
    * Postcondition: searches the fields of the indexed StructSymbol and returns
    *               the value if the identifier exists, else returns null.
    */
    public Symbol getIndexValue(int index, String identifier) {
        //test for ArrayOutOfBounds
        if (index >= array.getSize()) {
            System.err.println("ERROR, array: " + this.getIdentifier() + " bounds exceeded.");
            System.err.println("Exiting program.");
        }
        //first find the correct index
        ArrayList<Symbol> fields = variables[index].getFields();
        //now search the Structure to determine if a field exists
        for (Symbol field : fields) {
            if (field.getIdentifier().equals(identifier)) {
                return field;
            }
        }
        return null;
    }
    
    /*
    * Precondition: arguments must be an int and two Strings.
    * Postcondition: searches the fields of the indexed StructSymbol and sets
    *                the value if the identifier exists, returns a boolean
    *                true on success, false on fail.
    */
    public boolean setIndexValue(int index, String identifier, String value) {
        //check that index is within the bounds
        if (index >= array.getSize()) {
            System.err.println("ERROR, array: " + this.getIdentifier() + " bounds exceeded.");
            System.err.println("Exiting program.");
        }
        ArrayList<Symbol> fields = variables[index].getFields();
        for (Symbol field : fields) {
            if (field.getIdentifier().equals(identifier)) {
                field.setValue(value);
                return true;
            }
        }
        return false;
    }
    
    public int getSize() {
        return array.getSize();
    }
    
    public ArraySymbol getArray() {
        return array;
    }
    
    public void setArray(ArraySymbol array) {
        this.array = array;
        variables = new StructSymbol[array.getSize()];
        //initialize the indicies of the array as Structures
        for (int i = 0; i < variables.length; i++) {
            variables[i] = array.getStructure();
        }
    }
}
