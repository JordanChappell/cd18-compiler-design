/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: State.java
 * Description: State class holds a name, used in transition function of State Machine
 */

public class State {
    //member variables
    private final String name;
    
    /*
    * CONSTRUCTOR
    */
    public State(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
