/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: TIDLookup.java
 * Description: a lookup table of TID values to input into the Token
 */

import java.util.HashMap;
import java.util.Map;

public class TIDLookup {
    //member variables
    private static final Map<String, Integer> lookup = new HashMap<String, Integer>() {
        {
            put(",", 32);
            put("[", 33);
            put("]", 34);
            put("(", 35);
            put(")", 36);
            put("=", 37);
            put("+", 38);
            put("-", 39);
            put("*", 40);
            put("/", 41);
            put("%", 42);
            put("^", 43);
            put("<", 44);
            put(">", 45);
            put(":", 46);
            put("<=", 47);
            put(">=", 48);
            put("!=", 49);
            put("==", 50);
            put("+=", 51);
            put("-=", 52);
            put("*=", 53);
            put("/=", 54);
            put(";", 56);
            put(".", 57);
        }
    };
        
    /*
    *ACCESSOR METHODS
    */
    
    /*
    * Precondition: argurment must be a string
    * Postcondition: return the TID if the key exists, else returns -1
    */
    public int tid(String str) {
        if (lookup.containsKey(str)) {
            return lookup.get(str);
        }
        return -1;
    }
    
}
