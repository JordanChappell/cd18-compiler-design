/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: InputController
 * Description: class to interface with required files
 */

import java.util.*;
import java.io.*;

public class InputController {
    //member variables
    private File input;
    private Scanner reader;
    private OutputController outputController;
    
    /*
    * CONSTRUCTOR
    */
    public InputController(String filename) {
        input = new File(filename); 
        try {
            reader = new Scanner(input);
            reader.useDelimiter("");        //delimit with "" to return char by char
        } catch(FileNotFoundException e) {
            System.out.println("File " + input.getName() + " does not exist! "
            + "Terminating application.");
            System.exit(1);
        }
        outputController = new OutputController(filename);
    }
    
    /*
    * Precondition: N/A
    * Postcondition: returns the next token of the scanner (next char)
    */
    public String next() {
        String token = reader.next();
        while(token.contains("\r")) {           //eat the carriage returns - 
            token = reader.next();              //caused issues in my state machine (and i'm hungry)
        }
        outputController.writeFile(token);
        return token;  
    }
    
    public boolean hasNext() {
        return reader.hasNext();
    }    
}
