/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: OutputController.java
 * Description: a class to format outputs as required of the Compiler
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class OutputController {
    private String filename;
    private static boolean append = false;
    private boolean newline;
    private int printColCount;
    private int printLineCount;
    private int errorCount;
    
    /*
    * CONSTRUCTOR
    */
    public OutputController(String filename) {    
        this.filename = filename.replace(".txt", "") + "_programlisting.txt";
        newline = false;
        printColCount = 1;
        printLineCount = 0;
        errorCount = 0;
    }
    
    /*
    * METHODS
    */
    
    /*
    * Precondition: N/A
    * Postcondition: handles correct output of a token object to standard output
    */
    public void printToken(Token token) {
        if (token == null) {
            return;
        }
        //once the max columns have been reached reset counter and start a new line
        if (printColCount >= 12) {
            printColCount = 1;
            System.out.println();
        }
        //if the token has no lexeme simply increment the counter
        if (token.getStr() == null) {
            printColCount++;
        } else {        //otherwise calculate the column length required of the lexeme
            printColCount++;
            int length = token.getStr().length();
            if (length < 6) {
                printColCount ++;
            } else {
                int columns = (int) (length / (double)6);
                columns ++;
                printColCount += columns;
            }
        }
        System.out.print(token.shortString());      //print the token value
        //undefined token error print message
        if (token.value() == 62) {
            if (token.getStr().contains("\"")) {
                System.out.println("Lexical error: invalid TSTRG literal " + token.getStr());
            } else {
                System.out.println("Lexical error: invalid character " + token.getStr());
            }
            printColCount = 1;
        }
    }
    
    /*
    * Precondition: input must be a string
    * Postcondition: writes to the program listing, prepends appropriate line numbers
    */
    public void writeFile(String text) {
        try (FileWriter fw = new FileWriter(filename, append);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)){
            if (!append) {      //if the file has not been written to
                append = true;          //change the flag for appending
                printLineCount++;       //increment the line counter
                out.print(printLineCount + " ");    //print the first line number
            }
            if (newline) {      //if the newline flag is set
                printLineCount++;   //prints the next line number
                out.print(printLineCount + " ");
            }
            out.print(text);        //print the input
            if (text.equals("\n")) {    //this helps set the newline flag in the correct place
                newline = true;
            } else {
                newline = false;
            }
            out.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Terminating applicatiion");
            System.exit(1);
        }    
    }
    
    public void writeError(String error) {
        try (FileWriter fw = new FileWriter(filename, append);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)){
            out.print(error);        //print the error
            out.close();
            errorCount++;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Terminating applicatiion");
            System.exit(1);
        }       
    }
    
    public void writeErrorCount() {
        try (FileWriter fw = new FileWriter(filename, append);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)){
            out.println();
            out.println();
            if (errorCount > 0) {
                out.print("Found " + errorCount + " error(s)");
            } else {
                out.print("No errors found");
            }
            out.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Terminating applicatiion");
            System.exit(1);
        }  
    }
    
    public int getErrorCount() {
        return errorCount;
    }
}
