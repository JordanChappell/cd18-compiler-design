/*
 * Course: 
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: 
 * Description: 
 */

import java.util.ArrayList;

public class FuncSymbol extends Symbol {
    //member variables
    private ArrayList<Symbol> arguments;
    private boolean returnFlag;
    private boolean errorFlag;
    
    /*
    * CONSTRUCTORS
    */
    public FuncSymbol() {
        super();
        arguments = new ArrayList<>();
        errorFlag = true;
        returnFlag = false;
    }
     
    public FuncSymbol(String identifier) {
        super(identifier);
        arguments = new ArrayList<>();
        errorFlag = true;
        returnFlag = false;
    }   
    
    /*
    * SUBCLASS METHODS
    */
    public ArrayList<Symbol> getArguments() {
        return arguments;
    }
    
    public boolean addArguments(TreeNode tn) {
        if (arguments.contains((Symbol) tn.getSymbol())) {
            errorFlag = false;
        }
        if (tn.getValue() == TreeNode.NSDECL) {
            arguments.add(tn.getSymbol());
        }
        if (tn.getLeft() != null) {
            addArguments(tn.getLeft());
        }
        if (tn.getMiddle() != null) {
            addArguments(tn.getMiddle());
        }
        if (tn.getRight() != null) {
            addArguments(tn.getRight());
        }
        return errorFlag;
    }
    
    public void setReturnFlag() {
        returnFlag = true;
    }
    
    public boolean getReturnFlag() {
        return returnFlag;
    }
}
