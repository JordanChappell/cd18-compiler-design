/*
 * Course: COMP3290
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: ConditionList.java
 * Description: Lists of the possible input sets of a state machine.
 */

import java.util.Arrays;
import java.util.List;

public class ConditionList {
    //member variables (legal input sets)
    private static final String[] LETTERS = {"a","b","c","d","e","f","g","h",
        "i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    private static final String[] NUMBERS = {"0","1","2","3","4","5","6","7","8"
            ,"9"};
    private static final String[] OPERATORS = {"=","+","-","*","<",">"};
    private static final String[] DELIMITERS = {";","[","]",",","(",")","%","^"
        ,":"};
    private static final String[] SPACE = {" "};
    private static final String[] SLASH = {"/"};
    private static final String[] DASH = {"-"};
    private static final String[] QUOTE = {"\""};
    private static final String[] NEWLINE = {"\n"};
    private static final String[] TAB = {"\t"};
    private static final String[] EQUALS = {"="};
    private static final String[] DOT = {"."};
    private static final String[] EXCL = {"!"};
    private static final String[] ILLEGAL = {"`","~","@","#","$","&","_","{","}"
            ,"\\","|","'","?"};
    
    /*
    * ACCESSOR METHODS - return arrays as lists for Transition function
    */
    
    public List<String> letters() {
        return Arrays.asList(LETTERS);
    }
    
    public List<String> numbers() {
        return Arrays.asList(NUMBERS);
    }
    
    public List<String> operators() {
        return Arrays.asList(OPERATORS);
    }
    
    public List<String> delimiters() {
        return Arrays.asList(DELIMITERS);
    }
    
    public List<String> space() {
        return Arrays.asList(SPACE);
    }
    
    public List<String> slash() {
        return Arrays.asList(SLASH);
    }
    
    public List<String> dash() {
        return Arrays.asList(DASH);
    }
    
    public List<String> newline() {
        return Arrays.asList(NEWLINE);
    }
    
    public List<String> tab() {
        return Arrays.asList(TAB);
    }
    
    public List<String> quote() {
        return Arrays.asList(QUOTE);
    }
    
    public List<String> equals() {
        return Arrays.asList(EQUALS);
    }
    
    public List<String> dot() {
        return Arrays.asList(DOT);
    }
    
    public List<String> excl() {
        return Arrays.asList(EXCL);
    }
    
    public List<String> illegal() {
        return Arrays.asList(ILLEGAL);
    }
}
