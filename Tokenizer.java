/*
 * Course: 
 * Student No: 3230100
 * Student Name: Jordan Chappell
 * Class: Tokenizer.java
 * Description: takes a string buffer (arraylist) and returns a valid token
 */

import java.util.ArrayList;

public class Tokenizer {
    //take an input of an ArrayList (buffer)
    //and the expected token type (from laststate of the machine)
    private static final int[] tokenType = {58, 59, 60, 61, 62}; //tident, tilit, tflit, tstrg, tundf 
    private final TIDLookup lookup;
    
    /*
    * CONSTRUCTOR
    */
    public Tokenizer() {
        lookup = new TIDLookup();
    }
    
    /*
    * METHODS
    */
    
    /*
    * Precondition: arguments must be an ArrayList and an int
    * Postcondition: takes the ArrayList of strings and the index and builds a Token
    *                depending upon the tokenType value or the lookup value
    */
    public Token tokenize(ArrayList<String> buffer, int line, int col, int index) {
        String str = "";
        String s;
        int tid;
        //remove whitespace
        if (index != 3) {
            while (buffer.contains(" ")) {
                buffer.remove(" ");
            }
            while (buffer.contains("\t")) {
                buffer.remove("\t");
            }
        }
        for (int i = 0; i < buffer.size(); i++) {
            s = buffer.get(i);
            str += s; 
        }
        if (index == -1) {
            tid = lookup.tid(str);
            str = null;
            if (tid == -1) {
                //handle an array out of bounds problem, a tid -1 will access an out of bounds array index
                System.err.println("########ERROR, an INVALID TOKEN was detected, check your state machine!########");
                System.exit(1);
                return null;
            }
        } else {
            tid = tokenType[index];
        }
        return new Token(tid, line, col, str);
    }
    
    
    
}
